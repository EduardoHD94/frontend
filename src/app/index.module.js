(function() {
  'use strict';

  angular
    .module('uoux', ['ngAnimate', 'ngStorage', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr', 'angular-jwt']);

})();
