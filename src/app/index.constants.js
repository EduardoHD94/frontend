/* global malarkey:false, moment:false */
(function () {
  'use strict';

  angular
        .module('uoux')
        .constant('malarkey', malarkey)
        .constant('moment', moment)
        .constant("SERVER_URL", {
      "IPlocator": "http://ip-api.com/json",
      "LOCAL": "http://localhost:4000",  //"http://api.brickbienesraices.com", //
      //"TESTING": "http://api-testing.brickbienesraices.com",
      "PROD": "http://localhost:4000"
      //"PROD": "https://tinybackend.herokuapp.com/"
  });
})();
