(function(){
	'use strict';
	angular
		.module('uoux')
		.controller('IngresarController', ingresarCtrl);

	function ingresarCtrl($rootScope, $state, Auth, AuthService, jwtHelper, alertService, $localStorage, $log){
		var vm = this;
        vm.loginService = new AuthService();
        vm.auth = Auth;
		vm.userCrated ={
			failed: false,
            process: false,
			success: false
		};
        vm.user ={
			email: "",
			password: ""
		};
		vm.crearSesion = function(loginForm) {
            vm.userCrated.process= true;
            vm.loginService.email=vm.user.email;
            vm.loginService.password=vm.user.password;
            
            var dataToSend = angular.toJson(vm.loginService);
			AuthService.save(dataToSend)
                .$promise
                .then(function(data){
                    vm.userCrated.process= false;
                    vm.userCrated.success= true;
                    vm.auth.isLoggedIn = true;
                    vm.saveToken(data.auth_token);
                    $state.go('dashboard');
                    event.preventDefault();
				})
				.catch(function(response){
					if(response.status==401){
                        vm.userCrated.process= false;
						vm.userCrated.failed= true;
                        vm.alerta("danger","Your email o password is incorrect");
                        angular.element('html,body').animate({scrollTop:0},'slow');return false;
					}
				});
			loginForm.$setPristine();
            vm.UpdateError();
		};
		vm.UpdateError=function() {
			vm.userCrated.failed= false;
            vm.userCrated.process= false;
            vm.userCrated.success= false;
		};
        vm.saveToken = function(token) {
            $localStorage.currentUser = token;
            var tokenPayload = jwtHelper.decodeToken($localStorage.currentUser);
            var date = jwtHelper.getTokenExpirationDate($localStorage.currentUser);
            var bool = jwtHelper.isTokenExpired($localStorage.currentUser);
            $log.info(token);
            $log.info(tokenPayload);
            $log.info(date);
            $log.info(bool);
        }
        
        vm.logout = function() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            //$http.defaults.headers.common.Authorization = '';
        }
        
        vm.alerta = function(tipo, message) {
            alertService.add(tipo, message, 4000);
        };
        vm.myalert = function(msg){
            $log.info(msg);
            alert(msg);
        };
        $rootScope.closeAlert = alertService.closeAlert; 
	}
})();