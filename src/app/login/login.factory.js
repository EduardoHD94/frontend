(function(){
	'use strict';
	angular
		.module('uoux')
		.factory('AuthService',loginFunction);

	function loginFunction($resource,SERVER_URL)
	{
		var data = $resource( SERVER_URL.PROD +'/authenticate');
        return data;
        //return $resource( SERVER_URL.PROD +'/authenticate');
        //return $resource('http://localhost:4000/authenticate');
	}
})();