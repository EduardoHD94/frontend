(function(){
    'use strict';
    angular
        .module('uoux')
        .controller('DashboardController', DashboardCtrl);

    function DashboardCtrl(historialApi, alertService, $rootScope, jwtHelper, $localStorage, $log){
        var vm = this;
        vm.appInstanceCrated ={
            failed: false,
            process: false,
            success: false
        };
        var userInfo = jwtHelper.decodeToken($localStorage.currentUser);
        $log.info(userInfo);
        vm.app ={
            userid: userInfo.user_id,
            url: ""
        };

        vm.UpdateError=function() {
            vm.appInstanceCrated.failed= false;
            vm.appInstanceCrated.process= false;
            vm.appInstanceCrated.success= false;
        };

        vm.alerta = function(tipo, message) {
            alertService.add(tipo, message, 4000);
        };

        $rootScope.closeAlert = alertService.closeAlert; 
        
        vm.ligas = [];
        
        vm.sendInfo = function(appForm) {
            
            vm.appInstanceCrated.process= true;
            vm.myhistorialApi = new historialApi.data();
            vm.myhistorialApi.userid=vm.app.userid;
            vm.myhistorialApi.url=vm.app.url;
            vm.myhistorialApi.tinyurl="";
            $log.info(vm.app);
            var dataToSend = angular.toJson({"historial":vm.myhistorialApi});
            historialApi.new.save(dataToSend)
                .$promise
                .then(function(){
                    vm.appInstanceCrated.process= false;
                    vm.appInstanceCrated.success= true;
                    vm.alerta("success","Congratulations, your new app has been created successfully!");
                    vm.app.url=undefined;
                    vm.listInfo();
                    angular.element('html,body').animate({scrollTop:0},'slow');return false;
                })
                .catch(function(response){
                    if(response.status==422){
                        vm.appInstanceCrated.process= false;
                        vm.appInstanceCrated.failed= true;
                        vm.alerta("danger","Something went wrong, this is embarrassing, sorry");
                        angular.element('html,body').animate({scrollTop:0},'slow');return false;
                    }
                });
            appForm.$setPristine();
            vm.UpdateError();
        };
    
        vm.listInfo = function() {
            historialApi.list.query()
                .$promise
                .then(function(data){
                    vm.ligas = [];
                    angular.forEach(data, function(value){
                        if(value.userid === userInfo.user_id){
                            vm.littleInstance = new historialApi.data();
                            vm.littleInstance.id= value.id;
                            vm.littleInstance.userid= value.userid;
                            vm.littleInstance.url= value.url;
                            vm.littleInstance.tinyurl= value.tinyurl;
                            vm.littleInstance.created_at= value.created_at;
                            vm.littleInstance.updated_at= value.updated_at;
                            vm.ligas.push(vm.littleInstance);
                        }
                    });
                })
                .catch(function(response){
                    if(response.status==422){
                        vm.alerta("danger","Something went wrong, this is embarrassing, sorry");
                        angular.element('html,body').animate({scrollTop:0},'slow');return false;
                    }
                });
        };
    }
})();