(function(){
	'use strict';
	angular
		.module('uoux')
		.controller('SignupController', SignupCtrl);

	function SignupCtrl(appOwner, alertService, $rootScope, $log){
		var vm = this;
        vm.myappowner = new appOwner();
		vm.userCrated ={
			failed: false,
            process: false,
			success: false
		};
        vm.user ={
			name: "",
			email: "",
			password: "",
			password_confirmation: ""
		};
		vm.agreedTerms = false;
		vm.sendInfo = function(userForm) {
            vm.userCrated.process= true;
            
            vm.myappowner.name=vm.user.name;
            vm.myappowner.email=vm.user.email;
            vm.myappowner.password=vm.user.password;
            vm.myappowner.password_confirmation=vm.user.password_confirmation;
            
            var dataToSend = angular.toJson({"user":vm.myappowner});
			appOwner.save(dataToSend)
                .$promise
                .then(function(){
                    vm.userCrated.process = false;
                    vm.userCrated.success = true;
                    vm.alerta("success","Congratulations, now you are part of the team!");
                    vm.user.name=undefined;
                    vm.user.lastname=undefined;
                    vm.user.email=undefined;
                    vm.user.password=undefined;
                    vm.user.password_confirmation=undefined;
                    angular.element('html,body').animate({scrollTop:0},'slow');return false;
				})
				.catch(function(response){
					if(response.status==500){
                        vm.userCrated.process= false;
						vm.userCrated.failed= true;
                        vm.alerta("danger","Sorry, Can you try with other email");
                        angular.element('html,body').animate({scrollTop:0},'slow');return false;
					}
				});
			userForm.$setPristine();
            vm.UpdateError();
		};
		vm.UpdateError=function() {
			vm.userCrated.failed= false;
            vm.userCrated.process= false;
            vm.userCrated.success= false;
		};
        vm.alerta = function(tipo, message) {
            alertService.add(tipo, message, 4000);
        };
        vm.myalert = function(msg){
            $log.info(msg);
            alert(msg);
        };
        $rootScope.closeAlert = alertService.closeAlert; 
	}
})();