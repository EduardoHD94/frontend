(function(){
	'use strict';
	angular
		.module('uoux')
		.factory('appOwner',appOwnerFunction);

	function appOwnerFunction($resource,SERVER_URL)
	{
		return $resource( SERVER_URL.PROD +'/users/:id');
        //return $resource('http://localhost:4000/appowners/:id');
	}
})();