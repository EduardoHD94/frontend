(function(){
	'use strict';
	angular
		.module('uoux')
		.factory('historialApi',historialFunction);

    
	function historialFunction($resource,SERVER_URL, jwtHelper, $localStorage){	
		var oAuth = $localStorage.currentUser;
		//var configSave = { save: { method: 'PUT', headers: { Authorization: oAuth } } };
		var configQuery = { query: { method: 'GET', isArray: true, headers: { 'Authorization': oAuth } } };
        var configGet = { get: { method: 'GET', headers: { 'Authorization': oAuth } } };
		var configSave = { save: {method: 'POST', headers: { 'Authorization': oAuth } } };
        return{
            data: $resource(SERVER_URL.PROD+'/historials/'),
            new: $resource(SERVER_URL.PROD+'/historials/', '', configSave),
            list: $resource(SERVER_URL.PROD+'/historials/', '', configQuery),
            get: $resource(SERVER_URL.PROD+'/historials/:id', {id:'@myid'}, configGet) 
        };
	}
})();