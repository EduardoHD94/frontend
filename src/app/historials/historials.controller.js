(function(){
    'use strict';
    angular
        .module('uoux')
        .controller('DashboardController', DashboardCtrl);

    function DashboardCtrl(historialApi, alertService, $rootScope, jwtHelper, $localStorage, $log){
        var vm = this;
        vm.appInstanceCrated ={
            failed: false,
            process: false,
            success: false
        };
        var userInfo = jwtHelper.decodeToken($localStorage.currentUser);
        $log.info(userInfo);
        vm.app ={
            user_id: userInfo.user_id,
            url: ""
        };

        vm.UpdateError=function() {
            vm.appInstanceCrated.failed= false;
            vm.appInstanceCrated.process= false;
            vm.appInstanceCrated.success= false;
        };

        vm.alerta = function(tipo, message) {
            alertService.add(tipo, message, 4000);
        };

        $rootScope.closeAlert = alertService.closeAlert; 
        
        vm.appinstances = [];
        
        vm.sendInfo = function(appForm) {
            alert("GASIODA");
            vm.appInstanceCrated.process= true;
            vm.myhistorialApi = new historialApi.data();
            vm.myhistorialApi.userid=vm.app.user_id;
            vm.myhistorialApi.url=vm.app.url;
            
            var dataToSend = angular.toJson({"appinstance":vm.myhistorialApi});
            historialApi.new.save(dataToSend)
                .$promise
                .then(function(){
                    vm.appInstanceCrated.process= false;
                    vm.appInstanceCrated.success= true;
                    vm.listInfo();
                    vm.alerta("success","Congratulations, your new app has been created successfully!");
                    vm.app.name=undefined;
                    vm.app.url=undefined;
                    vm.app.about=undefined;
                    angular.element('html,body').animate({scrollTop:0},'slow');return false;
                })
                .catch(function(response){
                    if(response.status==422){
                        vm.appInstanceCrated.process= false;
                        vm.appInstanceCrated.failed= true;
                        vm.alerta("danger","Something went wrong, this is embarrassing, sorry");
                        angular.element('html,body').animate({scrollTop:0},'slow');return false;
                    }
                });
            appForm.$setPristine();
            vm.UpdateError();
        };
    /*
        vm.listInfo = function() {
            AppInstance.list.query()
                .$promise
                .then(function(data){
                    vm.appinstances = [];
                    angular.forEach(data, function(value){
                        if(value.appowner_id === userInfo.user_id){
                            vm.littleInstance = new AppInstance.data();
                            vm.littleInstance.id= value.id;
                            vm.littleInstance.name= value.name;
                            vm.littleInstance.url= value.url;
                            vm.littleInstance.html= value.html;
                            vm.littleInstance.about= value.about;
                            vm.littleInstance.status= value.status;
                            vm.littleInstance.created_at= value.created_at;
                            vm.littleInstance.updated_at= value.updated_at;
                            vm.appinstances.push(vm.littleInstance);
                        }
                    });
                })
                .catch(function(response){
                    if(response.status==422){
                        vm.alerta("danger","Something went wrong, this is embarrassing, sorry");
                        angular.element('html,body').animate({scrollTop:0},'slow');return false;
                    }
                });
        };*/
    }
})();