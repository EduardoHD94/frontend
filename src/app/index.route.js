(function() {
  'use strict';

  angular
    .module('uoux')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('login', {
        url: '/login',
        data : {isNotLogin : true },
        templateUrl: 'app/login/login.html'
      })
      .state('signup', {
        url: '/signup',
        data : {isNotLogin : true },
        templateUrl: 'app/signup/signup.html'
      })
      .state('dashboard', {
        url: "/dashboard",
        data : {requireLogin : true },
        templateUrl: "app/dashboard/dashboard.html"
      });

    $urlRouterProvider.otherwise('/');
  }

})();
