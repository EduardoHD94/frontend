(function() {
  'use strict';

  angular
    .module('uoux')
    .run(runBlock);

  /** @ngInject */
  //function runBlock($log, $scope, $rootScope, $state, $location, Auth) {
  function runBlock($log, $rootScope, $state, $location, Auth) {
      var unbind = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
      var shouldLogin = angular.isDefined(toState.data) && toState.data.requireLogin  && !Auth.isLoggedIn;
      var nowIsLogin = angular.isDefined(toState.data) && toState.data.isNotLogin  && Auth.isLoggedIn;
      //var shouldLogin = toState.data !== undefined && toState.data.requireLogin  && !Auth.isLoggedIn ;
  
      // NOT authenticated - wants any private stuff
      if(shouldLogin){
        $state.go('login');
        event.preventDefault();
        return;
      }
      
      // Authenticated - wants to go login or register
      if(nowIsLogin){
        $state.go('home');
        event.preventDefault();
        return;
      }
      
      /*// authenticated (previously) comming not to root main
      if(Auth.isLoggedIn){
        //var shouldGoToMain = fromState.name === "" && toState.name !== "main" ;
        var shouldGoToMain = fromState.name === "" && toState.name !== "dashboard" ;
          
        if (shouldGoToMain){
            $state.go('dashboard');
            event.preventDefault();
        } 
        return;
      }*/
      
      // UNauthenticated (previously) comming not to root public 
      //var shouldGoToPublic = fromState.name === "" && toState.name !== "public" && toState.name !== "login" ;
      var shouldGoToPublic = fromState.name === "" && toState.name !== "home" && toState.name !== "login" ;
        
      if(shouldGoToPublic){
          $state.go('home');
          event.preventDefault();
      } 
      
      // unmanaged
    });
    //this.$on('$destroy', unbind);
    //$log.debug('runBlock end');
  }

})();
