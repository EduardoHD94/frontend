(function() {
  'use strict';

  angular
    .module('uoux')
    .directive('acmeSlider', acmeSlider);

  /** @ngInject */
  function acmeSlider() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/slider/slider.html'
    };

    return directive;

  }

})();
