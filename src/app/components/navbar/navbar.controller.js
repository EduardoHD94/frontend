(function(){
    'use strict';
    angular
    .module('uoux')
    .controller('NavbarController', NavbarCtrl);
    function NavbarCtrl($rootScope, $state, Auth, AuthService, jwtHelper, $localStorage){
        var vm = this;
        vm.auth = Auth;
        vm.logout = function(){
            vm.auth.isLoggedIn=false;
            delete $localStorage.currentUser;
            $state.go('home');
            event.preventDefault();
        };
    }
})();