(function() {
  'use strict';

  angular
    .module('uoux')
    .directive('acmeLoader', acmeLoader);

  /** @ngInject */
  function acmeLoader() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/loader/loader.html'
    };

    return directive;

  }

})();
